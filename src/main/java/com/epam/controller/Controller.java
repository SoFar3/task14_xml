package com.epam.controller;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public interface Controller {

    void start() throws IOException, SAXException, ParserConfigurationException;

}

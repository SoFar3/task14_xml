package com.epam.controller;

import com.epam.model.Plane;
import com.epam.model.parser.DOMParser;
import com.epam.model.parser.SAXParser;
import com.epam.model.parser.StAXParser;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class ControllerImpl implements Controller {

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() throws IOException, SAXException, ParserConfigurationException {
        DOMParser domParser = new DOMParser("planes.xml");
        List<Plane> domPlanes = domParser.parse();
        System.out.println(domPlanes);

        SAXParser saxParser = new SAXParser("planes.xml");
        List<Plane> saxPlanes = saxParser.parse();
        System.out.println(saxPlanes);

        StAXParser staxParser = new StAXParser("planes.xml");
        List<Plane> staxPlanes = staxParser.parse();
        System.out.println(staxPlanes);
    }

}

package com.epam;

import com.epam.controller.ControllerImpl;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class App {

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        new ControllerImpl().start();
    }

}

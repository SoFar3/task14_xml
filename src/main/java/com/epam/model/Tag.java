package com.epam.model;

public class Tag {

    public static final String PLANES = "planes";
    public static final String PLANE = "plane";
    public static final String MODEL = "model";
    public static final String ORIGIN = "origin";
    public static final String CHARS = "chars";
    public static final String TYPE = "type";
    public static final String COMBAT_KIT = "combatKit";
    public static final String RADAR = "radar";
    public static final String ROCKET = "rocket";
    public static final String SIZE = "size";
    public static final String HEIGHT = "height";
    public static final String WIDTH = "width";
    public static final String LENGTH = "length";
    public static final String PRICE = "price";

}

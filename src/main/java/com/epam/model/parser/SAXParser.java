package com.epam.model.parser;

import com.epam.model.Chars;
import com.epam.model.CombatKit;
import com.epam.model.Plane;
import com.epam.model.Size;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.model.Tag.*;

public class SAXParser extends DefaultHandler implements Parser {

    private List<Plane> planes;
    private String elementValue;

    private File file;

    public SAXParser(String fileName) {
        this.file = new File("src/main/resources/" + fileName);
    }

    @Override
    public void startDocument() throws SAXException {
        planes = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case PLANES: break;
            case PLANE: planes.add(new Plane()); break;
            case CHARS: getLast().setChars(new Chars()); break;
            case COMBAT_KIT: getLast().getChars().setCombatKit(new CombatKit(false, 0)); break;
            case SIZE: getLast().setSize(new Size()); break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case MODEL: getLast().setModel(elementValue); break;
            case ORIGIN: getLast().setOrigin(elementValue); break;
            case TYPE:
                getLast()
                        .getChars()
                        .setType(elementValue);
                break;
            case RADAR:
                getLast()
                        .getChars()
                        .getCombatKit()
                        .setRadar(Boolean.parseBoolean(elementValue));
                break;
            case ROCKET:
                getLast()
                    .getChars()
                    .getCombatKit()
                    .setRocket(Integer.parseInt(elementValue));
                break;
            case HEIGHT:
                getLast()
                        .getSize()
                        .setHeight(Integer.parseInt(elementValue));
                break;
            case WIDTH:
                getLast()
                        .getSize()
                        .setWidth(Integer.parseInt(elementValue));
                break;
            case LENGTH:
                getLast()
                        .getSize()
                        .setLength(Integer.parseInt(elementValue));
                break;
            case PRICE:
                getLast().setPrice(Integer.parseInt(elementValue)); break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        elementValue = new String(ch, start, length);
    }

    @Override
    public List<Plane> parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(file, this);
        return planes;
    }

    private Plane getLast() {
        return planes.get(planes.size() - 1);
    }

}

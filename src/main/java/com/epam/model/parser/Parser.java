package com.epam.model.parser;

import com.epam.model.Plane;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public interface Parser {

    List<Plane> parse() throws ParserConfigurationException, IOException, SAXException;

}

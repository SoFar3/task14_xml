package com.epam.model.parser;

import com.epam.model.Chars;
import com.epam.model.CombatKit;
import com.epam.model.Plane;
import com.epam.model.Size;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser implements Parser {

    private File file;

    public DOMParser(String fileName) {
        this.file = new File("src/main/resources/" + fileName);
    }

    @Override
    public List<Plane> parse() throws ParserConfigurationException, IOException, SAXException {
        List<Plane> planes = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);

        Element root = doc.getDocumentElement();
        NodeList nodes = root.getElementsByTagName("plane");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String model = element.getElementsByTagName("model").item(0).getTextContent();
                String origin = element.getElementsByTagName("origin").item(0).getTextContent();
                String type = element.getElementsByTagName("type").item(0).getTextContent();
                Node combatKit = element.getElementsByTagName("combatKit").item(0);
                boolean radar = false;
                int rocket = 0;
                if (combatKit != null) {
                    radar = Boolean.parseBoolean(element.getElementsByTagName("radar").item(0).getTextContent());
                    rocket = Integer.parseInt(element.getElementsByTagName("rocket").item(0).getTextContent());
                }

                int height = Integer.parseInt(element.getElementsByTagName("height").item(0).getTextContent());
                int width = Integer.parseInt(element.getElementsByTagName("width").item(0).getTextContent());
                int length = Integer.parseInt(element.getElementsByTagName("length").item(0).getTextContent());

                int price = Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent());

                planes.add(
                        new Plane(
                                model, origin,
                                new Chars(
                                        type,
                                        new CombatKit(radar, rocket)),
                                new Size(height, width, length),
                                price));
            }
        }

        return planes;
    }

}

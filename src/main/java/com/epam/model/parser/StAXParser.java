package com.epam.model.parser;

import com.epam.model.Chars;
import com.epam.model.CombatKit;
import com.epam.model.Plane;
import com.epam.model.Size;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.model.Tag.*;

public class StAXParser implements Parser {

    private List<Plane> planes;

    private File file;

    public StAXParser(String fileName) {
        this.planes = new ArrayList<>();
        this.file = new File("src/main/resources/" + fileName);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public List<Plane> parse() throws IOException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        Plane plane = null;
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    xmlEvent = xmlEventReader.nextEvent();
                    if (startElement.getName().getLocalPart().equals(PLANE)) {
                        plane = new Plane();
                    } else if (startElement.getName().getLocalPart().equals(MODEL)) {
                        //xmlEvent = xmlEventReader.nextEvent();
                        plane.setModel(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(ORIGIN)) {
                        //xmlEvent = xmlEventReader.nextEvent();
                        plane.setOrigin(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(CHARS)) {
                        plane.setChars(new Chars());
                    } else if (startElement.getName().getLocalPart().equals(TYPE)) {
                        //xmlEvent = xmlEventReader.nextEvent();
                        plane.getChars().setType(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals(COMBAT_KIT)) {
                        plane.getChars().setCombatKit(new CombatKit());
                    } else if (startElement.getName().getLocalPart().equals(RADAR)) {
                        //xmlEvent = xmlEventReader.nextEvent();
                        plane.getChars().getCombatKit().setRadar(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(ROCKET)) {
                        //xmlEvent = xmlEventReader.nextEvent();
                        plane.getChars().getCombatKit().setRocket(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(SIZE)) {
                        plane.setSize(new Size());
                    } else if (startElement.getName().getLocalPart().equals(HEIGHT)) {
                        plane.getSize().setHeight(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(WIDTH)) {
                        plane.getSize().setWidth(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(LENGTH)) {
                        plane.getSize().setLength(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals(PRICE)) {
                        plane.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals(PLANE)) {
                        planes.add(plane);
                    }
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return planes;
    }

}

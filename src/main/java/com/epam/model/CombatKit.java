package com.epam.model;

public class CombatKit {

    private boolean radar;
    private int rocket;

    public CombatKit() {
    }

    public CombatKit(boolean radar, int rocket) {
        this.radar = radar;
        this.rocket = rocket;
    }

    public boolean isRadar() {
        return radar;
    }

    public void setRadar(boolean radar) {
        this.radar = radar;
    }

    public int getRocket() {
        return rocket;
    }

    public void setRocket(int rocket) {
        this.rocket = rocket;
    }

    @Override
    public String toString() {
        return "CombatKit{" +
                "radar=" + radar +
                ", rocket=" + rocket +
                '}';
    }

}

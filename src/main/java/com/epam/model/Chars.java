package com.epam.model;

public class Chars {

    private String type;
    private CombatKit combatKit;

    public Chars() {
    }

    public Chars(String type, CombatKit combatKit) {
        this.type = type;
        this.combatKit = combatKit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CombatKit getCombatKit() {
        return combatKit;
    }

    public void setCombatKit(CombatKit combatKit) {
        this.combatKit = combatKit;
    }

    @Override
    public String toString() {
        return "Chars{" +
                "type='" + type + '\'' +
                ", combatKit=" + combatKit +
                '}';
    }

}

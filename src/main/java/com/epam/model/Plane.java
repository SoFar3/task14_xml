package com.epam.model;

public class Plane {

    private String model;
    private String origin;
    private Chars chars;
    private Size size;
    private int price;

    public Plane() {
    }

    public Plane(String model, String origin, Chars chars, Size size, int price) {
        this.model = model;
        this.origin = origin;
        this.chars = chars;
        this.size = size;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars chars) {
        this.chars = chars;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "model='" + model + '\'' +
                ", origin='" + origin + '\'' +
                ", chars=" + chars +
                ", size=" + size +
                ", price=" + price +
                '}';
    }

}

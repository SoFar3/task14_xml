<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Planes</h2>
                <table border="1">
                    <tr bgcolor="#ef6c00">
                        <th>Model</th>
                        <th>Origin</th>
                        <th>Type</th>
                        <th>Radar</th>
                        <th>Rocket</th>
                        <th>Height</th>
                        <th>Width</th>
                        <th>Length</th>
                        <th>Price</th>
                    </tr>
                    <xsl:for-each select="planes/plane">
                        <tr>
                            <td><xsl:value-of select="model" /></td>
                            <td><xsl:value-of select="origin" /></td>
                            <td><xsl:value-of select="type" /></td>
                            <td><xsl:value-of select="radar" /></td>
                            <td><xsl:value-of select="rocket" /></td>
                            <td><xsl:value-of select="height" /></td>
                            <td><xsl:value-of select="width" /></td>
                            <td><xsl:value-of select="length" /></td>
                            <td><xsl:value-of select="price" /></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>